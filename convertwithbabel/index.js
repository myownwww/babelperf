var babel = require('babel-core');

module.exports = function(context, req) {
    context.log('Node.js HTTP trigger function processed a request. RequestUri=%s', req.originalUrl);

    if (req.query.name || (req.body && req.body.babelcode)) {
        var res = babel.transform(req.body.babelcode, {});
        context.res = {
            // status: 200, /* Defaults to 200 */
            body: res.code
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a name on the query string or in the request body"
        };
    }
    context.done();
};